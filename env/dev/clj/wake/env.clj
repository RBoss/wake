(ns wake.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [wake.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[wake started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[wake has shut down successfully]=-"))
   :middleware wrap-dev})
