(ns user
  (:require [mount.core :as mount]
            [wake.figwheel :refer [start-fw stop-fw cljs]]
            wake.core))

(defn start []
  (mount/start-without #'wake.core/repl-server))

(defn stop []
  (mount/stop-except #'wake.core/repl-server))

(defn restart []
  (stop)
  (start))


