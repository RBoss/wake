(ns ^:figwheel-no-load wake.app
  (:require [wake.core :as core]
            [devtools.core :as devtools]))

(enable-console-print!)

(devtools/install!)

(core/init!)
