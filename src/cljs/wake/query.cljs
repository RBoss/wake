(ns wake.query
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [re-frame.core :as rf]
            [reagent.core :as r]
            [wake.utils :as utils :refer [log]]
            [wake.scb :as scb]))

(defn get-query []
  (let [selections @(rf/subscribe [:get-query-selection])]
    (->> selections
         (filter #(not (empty? (second %))))
         (map (fn [item]
                { :code (first item ) :selection { :filter "item" :values (second item)}}))
         vec)))

(defn get-data []
  (go
    (let [query (get-query)
          subject-id @(rf/subscribe [:selected-subject])
          string-response (<! (scb/retrieve-data subject-id (.stringify js/JSON (clj->js query))))
          js-data (.parse js/JSON string-response)
          clj-data (js->clj js-data)
          data (utils/keywordize clj-data)]
      (rf/dispatch [:store-data data]))))

(defn list-item [id k v selections]
  (letfn [(handle-click! []
            (swap! selections update-in [k] not)
            (rf/dispatch [:set-query-selection [id (->> @selections (filter second) (map first) vec)]]))] 
    [:li { :href "#"
           :class (str "list-group-item list-group-item-action" (if (get @selections k) " active"))
          :on-click handle-click! } v]))

(defn variable-selection [{:keys [code text values valueTexts]} variable]
  (let [items (map vector values valueTexts)
        selections (->> items (map (fn [[k v]] [k false])) (into {}) r/atom)
        clear-selections! (fn [] (reset! selections (->> @selections
                                                        (map (fn [k] [(first k) false]))
                                                        (into {})))
                            (rf/dispatch [:set-query-selection [code []]]))
        all-selections! (fn [] (reset! selections (->> @selections
                                                       (map (fn [k] [(first k) true]))
                                                       (into {})))
                          (rf/dispatch [:set-query-selection [code (->> @selections (filter second) (map first) vec)]]))]
    (fn []
      (let [selected-count (->> @selections (filter second) count)]
        [:div.form-group.row.col-xs-4
         [:hr]
         [:div.col-form-label.col-xs-3
          [:label (clojure.string/capitalize text)
           [:span.tag.tag-default.tag-pill selected-count]]
          [:div
           [:button.btn.btn-link.all {:type "button" :on-click all-selections! } "all"]
           [:span "/"]
           [:button.btn.btn-link.clear {:type "button" :on-click clear-selections! } "clear"]]]
         [:div.col-xs-9
          [:ul.list-group.query-list 
           (for [[k v] items]
             ^{ :key k } [list-item code k v selections])]]]))))

(defn query-selection []
  (let [title @(rf/subscribe [:query-title])
        variables (utils/keywordize @(rf/subscribe [:query-variables]))]
    [:div.query-selection
     [:h5 title]
     [:form.form-horizontal
      (for [var variables]
        ^{ :key (str title (:code var)) } [variable-selection var])]
     [:hr]
     [:div#getdata [:input.btn.btn-primary { :value "get data" :type "button" :on-click get-data}]]
     [:hr]]))

(defn main []
  [query-selection])

