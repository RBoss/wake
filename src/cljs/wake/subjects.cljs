(ns wake.subjects
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [wake.utils :as utils]
   [wake.scb :as scb]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [cljs.core.async :refer [<!]]
   [ajax.core :refer [GET POST]]))

(defn retrieve-subjects [id fullpath]
  (go
    (let [result (<! (scb/retrieve-subjects fullpath))
          sub-subjects (->> result
                            utils/keywordize
                            (map #(assoc % :fullpath (str fullpath "/" (:id %)))))]
      (rf/dispatch [:store-subjects [id sub-subjects]]))))

(defn subject-selected [subject]
  (go 
    (let [fullpath (:fullpath subject)
          result (<! (scb/retrieve-subjects fullpath))]
      (rf/dispatch [:store-query result])
      (rf/dispatch [:set-selected-subject fullpath])
      (rf/dispatch [:reset-query-selection]))))

(defn subject-click [subject is-open]
  (let [{ :keys [type id fullpath]} subject]
    (case type
      "l" (do (swap! is-open not)
              (retrieve-subjects id fullpath))
      "t" (subject-selected subject))))

(defn main []
  (letfn [(subject-row [subject]
            (let [is-open (r/atom false)
                  is-active (= @(rf/subscribe [:selected-subject]) (:fullpath subject))]
              (fn [subject]
                (let [{ :keys [id fullpath text type]} subject
                      is-active (= @(rf/subscribe [:selected-subject]) (:fullpath subject))]
                  [:li.list-group-item.subject-row { :class (if is-active "active")}
                   [:div.subject-title { :on-click #(subject-click subject is-open) }
                    (if (= type "l")
                      [:span.fa { :class (if @is-open "fa-minus-square-o" "fa-plus-square-o")}]) text]
                   (if @is-open
                     [subjects-table (keyword id)])]))))

          (subjects-table [key]
            (let [subjects @(rf/subscribe [:get-subjects key])]
              [:ul.list-group.subjects { :class (name key)}
               (for [subject subjects]
                 ^{:key (:id subject)}[subject-row subject])]))]
    [subjects-table :base]))

(defonce init
  (retrieve-subjects :base ""))
