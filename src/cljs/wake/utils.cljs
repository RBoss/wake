(ns wake.utils
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as r]
            [cljs.core.async :refer [chan <! close!]]
            [goog.net.XhrIo]
            [re-frame.core :as rf]
            [ajax.core :refer [GET POST]]))

(def log (.-log js/console))

(defn http-get [url params]
  (log url)
  (let [ch (chan 1)]
    (GET url { :params params
              :format :json
              :reponse-format :text
              :handler
              (fn [res]
                (go (>! ch res)
                    (close! ch)))})
    ch))

(defn http-post [url body]
  (log (clj->js body))
  (let [ch (chan 1)]
    (POST url { :params (clj->js body)
               :format :json
               :reponse-format :json
               :handler
               (fn [res]
                 (go (>! ch res)
                     (close! ch)))})
    ch))

(defn indices [pred coll]
  (keep-indexed #(when (pred %2) %1) coll))

(defn index-of [pred coll]
  (first (indices pred coll)))

(defn keywordize [data] 
  (vec (map clojure.walk/keywordize-keys data)))

(defn selected-options [target]
  (let [options (.-options target)]
    (->> options
         (filter #(.-selected %)))))

(defn debugger []
  (js/eval "debugger"))

(defn obj->vec [obj]
  "Put object properties into a vector"
  (vec (map (fn [k] {:key k :val (aget obj k)}) (.keys js/Object obj))))
