(ns wake.charts.base
  (:require
   [reagent.core :as reagent]))

(defn chart-component [display-name data did-mount did-update]
  (let [dom-node (reagent/atom nil)]
    (reagent/create-class
     { :component-did-mount
      (fn [this]
        (let [node (reagent/dom-node this)]
          (print "component-did-mount" node)
          (did-mount node)
          (reset! dom-node node)))
      :component-did-update
      (fn [this]
        (let [[_ args] (reagent/argv this)
              node (reagent/dom-node this)]
          (print "component-did-update" args)
          @dom-node
          (if args
            (did-update node args))))
      :display-name display-name
      :reagent-render
      (fn []
        @dom-node
        [:svg.chart])}))) 
