(ns wake.charts.line
    (:require
     [reagent.core :as reagent]
     [re-frame.core :as rf]
     [cljsjs.d3]))
  
(def margin {:top 20 :right 30 :bottom 30 :left 40})
(def width (- 960 (:left margin) (:right margin)))
(def height (- 300 (:top margin) (:bottom margin)))
  
(defn mount-chart [node]
  (print "mounting")
  (let [chart
          (-> (js/d3.select node)
              (.attr "width" (+ width (:right margin) (:left margin)))
              (.attr "height" (+ height (:top margin) (:bottom margin)))
              (.append "g")
              (.attr "transform" (str "translate(" (:left margin) "," (:top margin) ")")))]
    (-> chart
        (.append "g")
        (.attr "class" "axis axis--x")
        (.attr "transform" (str "translate(0, " height ")")))
    (-> chart
        (.append "g")
        (.attr "class" "axis axis--y")
        (.append "text")
        (.attr "y" -20)
        (.attr "x" -4)
        (.attr "dy" "0.81em")
        (.text ""))))
  
(defn update-chart [dt]
  (print "updating:" dt)
  (let [data (clj->js dt)
        highest-value (->> dt (map (comp js/parseFloat first :values)) vec (apply max))
        lowest-value (->> dt (map (comp js/parseFloat first :values)) vec (apply min))
        x-axis-values (->> dt
                           (map (fn [d] (first (:key d))))
                           vec)
        y
        (-> js/d3
            .scaleLinear
            (.domain #js [0 (* 2 highest-value)])
            (.rangeRound #js [height 0]))
        x (-> js/d3
              .scaleBand
              (.domain (clj->js x-axis-values))
              (.rangeRound #js [0 width]))
        valueLine (-> js/d3
                      .line
                      (.x (fn [d] (x d.key)))
                      (.y (fn [d] (y d.values))))
        xAxis (-> js/d3
                  (.axisBottom x))
        chart (-> (js/d3.select ".chart")
                  (.select "g"))
        barWidth (.bandwidth x)
        transition (-> js/d3
                       .transition
                       (.duration 400)
                       (.delay (fn [d i] (* i 400))))
        line (-> chart
                (.append "path")
                (.data data)
                (.attr "class" "linechart")
                (.attr "d" (valueLine data)))]
        ;; bar-update (-> bar
        ;;                (.attr "transform" (fn [d i] (str "translate(" (x d.key) ", 0)"))))
        ;; bar-enter  (-> bar
                  ;;     .enter
                  ;;     (.append "g")
                  ;;     (.attr "class" "line")
                  ;;     (.attr "transform" (fn [d i] (str "translate(" (x d.key) ", 0)"))))
        
    ;; (-> bar-enter
    ;;     (.append "rect")
    ;;     (.attr "class" "bar")
    ;;     (.attr "y" height)
    ;;     (.merge (-> bar-update (.select "rect")))
  
    ;;     (.transition transition)
    ;;     (.attr "width" barWidth)
    ;;     (.attr "y" (fn [d] (y d.values)))
    ;;     (.attr "height" (fn [d] (- height (y d.values)))))
  
    ;; (-> bar-enter
    ;;     (.append "text")
    ;;     (.merge (-> bar-update (.select "text")))
    ;;     (.attr "x" (/ barWidth 2))
    ;;     (.attr "y" (fn [d] (+ (y (first d.values)) 3)))
    ;;     (.attr "dy" ".75em")
    ;;     (.text (fn [d] d.values)))
    ;; (-> bar
    ;;     .exit
    ;;     (.transition transition)
    ;;     (.attr "height" 0)
    ;;     .remove)
    (-> chart
        (.selectAll "g.axis--x")
        (.call xAxis))
    (-> chart
        (.select "g.axis--y")
        (.call (-> js/d3
                   (.axisLeft y)
                   (.ticks 10)))
        (.select "text")
        (.text "Age"))))
  
(defn line-chart [data]
  (let [dom-node (reagent/atom nil)]
    (reagent/create-class
     { :component-did-mount (fn [this]
                              (let [node (reagent/dom-node this)]
                                (print "component-did-mount" node)
                                (mount-chart node)
                                (reset! dom-node node)))
      :component-did-update (fn [this]
                              (let [[_ args] (reagent/argv this)]
                                (print "component-did-update" args)
                                @dom-node
                                (if args
                                  (update-chart args))))
      :display-name "line chart"
      :reagent-render (fn []
                        @dom-node
                        [:svg.chart])}))) 
  
(defn render []
  (let [data (get (into {} @(rf/subscribe [:data])) "data")]
    [line-chart data])) 
  
