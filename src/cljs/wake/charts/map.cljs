(ns wake.charts.map
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as rf]
   [cljsjs.d3]
   [cljsjs.topojson]))

(def width 400)
(def height 500)

(def maxPop 1000)
(def minPop 0)

(defn data-retrieved [err result]
  (let [subunits (js/topojson.feature result result.objects.subunits)
        features subunits.features
        color (-> js/d3
                   .scaleLinear
                   (.domain #js [minPop maxPop])
                   (.range #js ["#EEE" "cornflowerblue"]))
        projection (-> js/d3
                       .geoAlbers
                       (.center #js [0 62.4])
                       (.rotate #js [-15.4 0])
                       (.scale 2100)
                       (.translate #js [(/ width 2) (/ height 2)]))
        path (-> js/d3
                 .geoPath
                 (.projection projection))
        svg (-> (js/d3.select "svg.map-chart"))]
    (-> svg
        (.append "path")
        (.datum subunits)
        (.attr "d" path))
    (-> svg
        (.selectAll ".subunit")
        (.data features)
        .enter
        (.append "path")
        (.attr "class" (fn [d] (str "subunit " d.id)))
        (.attr "d" path)
        (.style "fill" (fn [d] (color (rand-int 1000)))))

    (-> svg
        (.append "path")
        (.datum (js/topojson.mesh result result.objects.subunits (fn [a b] (not (= a b))))) 
        (.attr "d" path)
        (.attr "class" "subunit-boundary"))

    (print "done!")))

(defn mount-chart [node]
  (print "mounting"))

(defn update-chart [node]
  (.json js/d3 "js/sweden-cities.json" data-retrieved))

(defn map-chart [data]
  (let [dom-node (reagent/atom nil)]
    (reagent/create-class
     { :component-did-mount (fn [this]
                              (let [node (reagent/dom-node this)]
                                (print "component-did-mount map" node)
                                (mount-chart node)
                                (reset! dom-node node)))
      :component-did-update (fn [this]
                              (let [[_ args] (reagent/argv this)]
                                (print "component-did-update" args)
                                @dom-node
                                (if args
                                  (update-chart args))))
      :display-name "map-chart"
      :reagent-render (fn []
                        @dom-node
                        [:svg.map-chart { :width width :height height}])})))

(defn render []
  [map-chart {}])
