(ns wake.scb
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [wake.utils :as utils]
   [cljs.core.async :refer [<!]]
   [ajax.core :refer [GET POST]]))

(def root-url "http://api.scb.se/ov0104/v1/doris/en/ssd/")

(defn retrieve-subjects [id]
  (let [url (str root-url id)]
    (utils/log url)
    (utils/http-get url nil)))

(defn retrieve-data [id query]
  (let [url (str "/scb?path=" id)]
    (utils/http-get url { :query query })))

;; (let [res (retrieve-data "/BE/BE0101/BE0101B/BefolkningMedelAlder")
;;       body (:body res)]
;;   (utils/log res))
