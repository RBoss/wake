(ns wake.stat
  (:require
   [wake.events]
   [wake.subjects :as subjects]
   [wake.query :as query]
   [wake.result :as result]
   [re-frame.core :as rf]
   [cljsjs.d3]))

(defn main []
  [:div
   [:div#subjects.col-xs-3.navigation
    [subjects/main]]
   [:div.col-xs-9
    [query/main]
    [result/main]]])
