(ns wake.chart
  (:require
   [wake.charts.bar :as bar-chart]
   [wake.charts.line :as line-chart]
   [wake.charts.map :as map-chart]))

(defmulti render-chart (fn [chart] (:type chart)))

(defmethod render-chart :bar
 [chart
  [bar-chart/render]])

(defmethod render-chart :line
  [chart]
  [line-chart/render])

(defmethod render-chart :map
  [chart]
  [map-chart/render])



