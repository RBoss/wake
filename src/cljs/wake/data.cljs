
(ns wake.data)

(def db-data
  { :query {}
   :tree
   [{:id "BE",
     :type "l",
     :text "Population",
     :subjects
     [{:id "BE/BE0701",
       :type "l",
       :text "Demographic Analysis (Demography)"}
      {:id "BE/BE0001", :type "l", :text "Name statistics"
       :subjects [
                  {:id "BE/BE0001/BE0001T04Ar", :type "t", :text 
                   "Newborns, first name … ar. Year 1998 - 2016"
                   , :updated "2017-01-31T09:30:00"}
                  {:id "BE/BE0001/BE0001T04BAr", :type "t", :text 
                   "Newborns, first name … me. Year 2004 - 2016"
                   , :updated "2017-01-31T09:30:00"}
                  {:id "BE/BE0001/BE0001T05AR", :type "t", :text 
                   "Newborns, the 10 and … ar. Year 1998 - 2016"
                   , :updated "2017-01-31T09:30:00"}
                  {:id "BE/BE0001/BE0001T06AR", :type "t", :text 
                   "First names normally … ar. Year 1999 - 2016"
                   , :updated "2017-02-28T09:30:00"}
                  {:id "BE/BE0001/BE0001T03Ar", :type "t", :text 
                   "Last names, the 10 a … ar. Year 1980 - 2016"
                   , :updated "2017-02-28T09:30:00"}]}

      {:id "BE/BE0401", :type "l", :text "Population projections"}
      {:id "BE/BE0101", :type "l", :text "Population statistics"}]}
    {:id "EN",
     :type "l",
     :text "Energy",
     :subjects
     [{:id "EN/EN0107",
       :type "l",
       :text "Monthly fuel, gas and inventory statistics"}
      {:id "EN/EN0105",
       :type "l",
       :text
       "Annual energy statistics (electricity, gas, district heating)"}]}
    {:id "HA",
     :type "l",
     :text "Trade in goods and services",
     :subjects []}
    {:id "JO",
     :type "l",
     :text "Agriculture, forestry and fishery",
     :subjects []}
    {:id "ME", :type "l", :text "Democracy", :subjects []}
    {:id "NR", :type "l", :text "National accounts", :subjects []}
    {:id "OE", :type "l", :text "Public finances", :subjects []}
    {:id "TK",
     :type "l",
     :text "Transport and communications",
     :subjects []}
    {:id "AM", :type "l", :text "Labour market", :subjects []}
    {:id "BO",
     :type "l",
     :text "Housing, construction and building",
     :subjects []}
    {:id "FM", :type "l", :text "Financial markets", :subjects []}
    {:id "HE", :type "l", :text "Household finances", :subjects []}
    {:id "LE", :type "l", :text "Living conditions", :subjects []}
    {:id "MI", :type "l", :text "Environment", :subjects []}
    {:id "NV", :type "l", :text "Business activities", :subjects []}
    {:id "PR", :type "l", :text "Prices and Consumption", :subjects []}
    {:id "UF", :type "l", :text "Education and research", :subjects []}]})
