(ns wake.result
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as rf]
   [wake.chart :as chart]))

(defn map-display []
  [:div
   [:h4 "Map"]
   [:div#map
    [chart/render-chart {:type :map}]]])

(defn chart-display []
  [:div
   [:h4 "Line chart"]
   [:div#linechart
    [chart/render-chart {:type :line}]]
   [:h4 "Bar chart"]
   [:div#barchart
    [chart/render-chart {:type :bar}]]])

(defn table-row [row]
  [:tr
   (for [value (vec (concat (:key row) (:values row)))]
     ^{:key value} [:td value])])

(defn table-display []
  (let [data (into {} @(rf/subscribe [:data]))
        columns (get data "columns")
        rows (get data "data")]
    [:div
     [:h4 "Table"]
     [:table.table.table-sm.table-hover.table-bordered
      [:thead
       [:tr
        (for [col columns]
          ^{:key (:text col)} [:th (clojure.string/capitalize (:text col))])]]
      [:tbody
       (for [row rows]
         ^{:key (:key row)} [table-row row])]]]))

(defn main []
  [:div
   [map-display]
   [:hr]
   [chart-display]
   [:hr]
   [table-display]])
