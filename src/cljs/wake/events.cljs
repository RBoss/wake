(ns wake.events
  (:require [re-frame.core :as rf]
            [wake.utils :as utils]))

(defonce state
  {})

(rf/reg-event-db
 :initialize
 (fn [_ _]
   state))

(rf/reg-event-db
 :store-data
 (fn [db [_ data]]
   (assoc-in db [:data] data)))

(rf/reg-event-db
 :store-query
 (fn [db [_ query-data]]
   (assoc-in db [:query] query-data)))

(rf/reg-event-db
 :store-subjects
 (fn [db [_ [id subjects]]]
   (assoc-in db [:subjects (keyword id)] subjects)))

(rf/reg-event-db
 :set-selected-subject
 (fn [db [_ id]]
   (assoc-in db [:selected-subject] id)))

(rf/reg-event-db
 :set-query-selection
 (fn [db [_ [id selections]]]
   (assoc-in db [:query-selection id] selections)))

(rf/reg-event-db
 :reset-query-selection
 (fn [db [_ _]]
   (assoc-in db [:query-selection] {})))

;; (rf/reg-event-fx
;;  :get-data
;;  (fn [cofx [_ subject-id]]))

(rf/reg-sub
 :db
 (fn [db _]
   db))

(rf/reg-sub
 :selected-subject
 (fn [db _]
   (:selected-subject db)))

(rf/reg-sub
 :get-query-selection
 (fn [db _]
   (:query-selection db)))

(rf/reg-sub
 :get-subjects
 (fn [db [_ id]]
   (get-in db [:subjects id])))

(rf/reg-sub
 :query
 (fn [db _]
   (:query db)))

(rf/reg-sub
 :query-title
 (fn [_]
   (rf/subscribe [:query]))
 (fn [query]
   (get query "title")))

(rf/reg-sub
 :query-variables
 (fn [_]
   (rf/subscribe [:query]))
 (fn [query]
   (get query "variables")))

(rf/reg-sub
 :data
 (fn [db _]
   (:data db)))
