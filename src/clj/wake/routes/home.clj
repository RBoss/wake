(ns wake.routes.home
  (:require [wake.layout :as layout]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [clojure.core.async :as async :refer [go chan >! >!! <! <!! close! timeout]]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [ajax.core :as ajax]))

(def root-url "http://api.scb.se/ov0104/v1/doris/en/ssd/")

(defn home-page []
  (layout/render "home.html"))

(defn http-get [url]
  (let [ch (chan)]
    (ajax/POST url { :body (clojure.data.json/json-str { :query [], :response { :format "json"}})
                    :format :text
                    :reponse-format :json
                    :error-handler
                    (fn [res]
                      (>!! ch res))
                    :handler
                    (fn [res]
                      (>!! ch res))})
    ch))

(defn http-post [url query]
  (println (str "---- " (clojure.data.json/json-str { :query query :response { :format "json"}})))
  (println (str "---- " (clojure.data.json/json-str { :query [] :response { :format "json"}})))
  (client/post url
               {
                :body (clojure.data.json/json-str { :query query :response { :format "json"}})
                :content-type :json
                :accept :json}))

;; (http-post "http://api.scb.se/ov0104/v1/doris/en/ssd/BE/BE0001/BE0001T06AR")

;; (let [url "http://api.scb.se/ov0104/v1/doris/en/ssd/BE/BE0101/BE0101B/BefolkningMedelAlder"
;;       res (http-post url)
;;       body (clojure.string/replace-first (:body res) "?" "")]
;;   (println body))


(defroutes home-routes
  (GET "/" []
       (home-page))
  (GET "/docs" []
       (-> (response/ok (-> "docs/docs.md" io/resource slurp))
           (response/header "Content-Type" "text/plain; charset=utf-8")))
  (GET "/scb" [path query]
       (let [url (str root-url path)
             res (http-post url (json/read-str query))
             body (clojure.string/replace-first (:body res) "?" "")]
         (println "scb/ url: " url " query: " query)
         (def w query)
         (->
          (response/ok body)
          (response/header "Content-Type" "text/plain")))))
