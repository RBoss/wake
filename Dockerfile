FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/wake.jar /wake/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/wake/app.jar"]
